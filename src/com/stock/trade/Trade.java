package com.stock.trade;

/**
 * Model class for trade buy and sell.
 * @author AMOL
 *
 */

public class Trade {

	public double buy;
	public double sell;

	public Trade(double buy, double sell) {
		this.buy = buy;
		this.sell = sell;
	}
	
	public Trade() {
		
	}
	

	public double getBuy() {
		return buy;
	}

	public void setBuy(double buy) {
		this.buy = buy;
	}

	public double getSell() {
		return sell;
	}

	public void setSell(double sell) {
		this.sell = sell;
	}

	@Override
	public String toString() {
		return "Trade [buy=" + buy + ", sell=" + sell + "]";
	}

}
