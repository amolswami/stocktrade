package com.stock.trade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 
 * @author AMOL
 *
 */
public class StockTrader {

	/**
	 * This method gave the lowest buy value and highest sell value.
	 * 
	 * @return Trade buy and sell value after compairing both value gave max value.
	 * @value of buy and sell
	 */
	
	public Trade buyLowSellHigh() {

		List<Trade> trades = new ArrayList<Trade>();

		String str = "9.20 8.03 10.02 8.08 8.14 8.10 8.31 8.28 8.35 8.34 8.39 8.45 8.38 8.38 8.32 8.36 8.28 8.28 8.38 8.48 8.49 8.54 8.73 8.72 8.76 8.74 8.87 8.82 8.81 8.82 8.85 8.85 8.86 8.63 8.70 8.68 8.72 8.77 8.69 8.65 8.70 8.98 8.98 8.87 8.71 9.17 9.34 9.28 8.98 9.02 9.16 9.15 9.07 9.14 9.13 9.10 9.16 9.06 9.10 9.15 9.11 8.72 8.86 8.83 8.70 8.69 8.73 8.73 8.67 8.70 8.69 8.81 8.82 8.83 8.91 8.80 8.97 8.86 8.81 8.87 8.82 8.78 8.82 8.77 8.54 8.32 8.33 8.32 8.51 8.53 8.52 8.41 8.55 8.31 8.38 8.34 8.34 8.19 8.17 8.16";

		List<Double> prices = Stream.of(str.split(" ")).map(Double::parseDouble).collect(Collectors.toList());

		for (int i = 0; i < prices.size() - 2; i++) {
			double buy = prices.get(i);
			for (int j = i + 2; j < prices.size(); j++) {
				double sell = prices.get(j);
				trades.add(new Trade(buy, sell));
				}
			}
		
		Optional<Trade> compare = trades.stream().collect(Collectors
				.maxBy((t1, t2) -> Double.valueOf(t1.sell - t1.buy).compareTo(Double.valueOf(t2.sell - t2.buy))));
		System.out.println(compare.get());
		return compare.get();
    }

	/**
	 * main method
	 */
	public static void main(String[] args) {

		StockTrader stockTrader = new StockTrader();
		stockTrader.buyLowSellHigh();

	}

}
