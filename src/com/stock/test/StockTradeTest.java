package com.stock.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;

import com.stock.trade.StockTrader;
import com.stock.trade.Trade;

/**
 * Junit test cases for possitive and negative scenerios.
 * 
 * @author AMOL
 *
 */

public class StockTradeTest {

	@Test
	public void testForBuyLowSellHigh() {

		StockTrader stockTrader = new StockTrader();
		Trade stockTrade = stockTrader.buyLowSellHigh();
		String str = "Trade [buy=8.03, sell=9.34]";
		String str1 = stockTrade.toString();
		assertNotNull(stockTrade);
		assertEquals(str, str1);

	}

	@Test
	public void testForBuyLowSellHighNegative() {

		StockTrader stockTrader = new StockTrader();
		Trade stockTrade = stockTrader.buyLowSellHigh();
		String str = "Trade [buy=8.01, sell=9.47]";
		String str1 = stockTrade.toString();
		assertNotEquals(str, str1);

	}

}
